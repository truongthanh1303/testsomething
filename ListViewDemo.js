import React, { Component } from 'react';
import {
  ActivityIndicator,
  FlatList,
  ListView,
  Text,
  View,
  ScrollView,
} from 'react-native';

export default class ListViewDemo extends Component {
  constructor() {
    super();
    this.state = {
      data: {},
      isLoading: true,
    };
    this.page = 1;
  }

  componentDidMount() {
    this.makeRemoteRequest();
  }

  makeRemoteRequest(cb) {
    console.log('makeRemoteRequest')
    const url = `https://randomuser.me/api/?seed=1&page=${this.page}&results=20`;

    fetch(url)
      .then(res => res.json())
      .then(res => {
        alert(1)
        this.setState({
          data: this.page === 1 ? res.results : [...this.state.data, ...res.results],
          isLoading: false,
        });
      })
      .catch(error => {
        // this.setState({ error, loading: false });
      });
  }

  handleLoadmore = () => {
    this.page += 1;
    this.setState({
      isLoading: true
    }, () => {
      this.makeRemoteRequest();
    });
  }

  renderRow = (item) => {
    return (
      <View style={{
        marginTop: 10,
        marginBottom: 10,
        padding: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#ccc'
      }}>
        <Text>
          {
            `${item.name.first} ${item.name.last}`
          }
        </Text>
      </View>
    )
  }

  renderHeader = () => {
    return (
      <View style={{
        height: 100,
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: 'green',
      }}>
        <Text>Header</Text>
      </View>
    )
  }

  renderFooter = () => {
    if (this.state.isLoading) {
      return (
        <ActivityIndicator
          size="large"
        />
      )
    }
    return null;
  }

  render() {
    const dataSource = new ListView.DataSource({ rowHasChanged: (a, b) => a != b }).cloneWithRows(this.state.data);
    console.log('dataSource: ', this.state.data)
    return (
      <ListView
        style={{flex: 1}}
        dataSource={dataSource}
        initialListSize={10}
        onEndReached={this.handleLoadmore}
        onEndReachedThreshold={0.5}
        pageSize={10}
        renderRow={this.renderRow}
        renderHeader={this.renderHeader}
        renderFooter={this.renderFooter}
      />
    );
    // return (
    //   <FlatList
    //     style={{flex: 1}}
    //     data={this.state.data}
    //     renderItem={this.renderRow}
    //     initialNumToRender={20}
    //     keyExtractor={(item, idx) => idx}
    //     ListHeaderComponent={this.renderHeader}
    //     ListFooterComponent={this.renderFooter}
    //     onEndReached={this.handleLoadmore}
    //     onEndReachedThreshold={0.5}
    //   />
    // )
  }
}
