import { Navigation } from 'react-native-navigation';

import { registerScreens } from './screens';

registerScreens();
Navigation.startSingleScreenApp({
  screen: {
    screen: 'example.ListViewDemo',
    overrideBackPress: true,
  },
  animationType: 'fade',
  appStyle: {
    orientation: 'portrait',
  },
});
