import { Navigation } from 'react-native-navigation';
import FirstScreen from './FirstScreen';
import ListViewDemo from './ListViewDemo';

export function registerScreens() {
  Navigation.registerComponent('example.FirstScreen', () => FirstScreen);
  Navigation.registerComponent('example.ListViewDemo', () => ListViewDemo);
}
